package com.example.craps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView die_first, die_second;
    private TextView message_v;
    private int val_1, val_2;
    private int total, point;
    private int roll_count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        loan_amount = findViewById(R.id.loan_text);
//        interest_amount = findViewById(R.id.interest_text);
        die_first = findViewById(R.id.image1);
        die_second = findViewById(R.id.image2);
        message_v = findViewById(R.id.message);
    }

    public void roll(View view){
        val_1 = new Random().nextInt(6) + 1;
        val_2 = new Random().nextInt(6) + 1;
        total = val_1 + val_2;
        switch (val_1){
            case 1:
                die_first.setImageResource(R.mipmap.die1);
                break;
            case 2:
                die_first.setImageResource(R.mipmap.die2);
                break;
            case 3:
                die_first.setImageResource(R.mipmap.die3);
                break;
            case 4:
                die_first.setImageResource(R.mipmap.die4);
                break;
            case 5:
                die_first.setImageResource(R.mipmap.die5);
                break;
            case 6:
                die_first.setImageResource(R.mipmap.die6);
                break;
        }
        switch (val_2){
            case 1:
                die_second.setImageResource(R.mipmap.die1);
                break;
            case 2:
                die_second.setImageResource(R.mipmap.die2);
                break;
            case 3:
                die_second.setImageResource(R.mipmap.die3);
                break;
            case 4:
                die_second.setImageResource(R.mipmap.die4);
                break;
            case 5:
                die_second.setImageResource(R.mipmap.die5);
                break;
            case 6:
                die_second.setImageResource(R.mipmap.die6);
                break;
        }

        if (roll_count == 0){
            if(total == 2 || total == 3 || total == 12){
                message_v.setText("Craps! You Lose!");
                roll_count = 0;
            }
            else if(total == 7 || total == 11){
                message_v.setText("You Won Your First Roll!");
                roll_count = 0;
            }
            else{
                point = total;
                roll_count++;
                message_v.setText("Point is set to " + Integer.toString(total) + "!");
            }
        }
        else{
            if (total == point){
                message_v.setText("You won!");
                roll_count = 0;
            }
            else if(total == 7){
                message_v.setText("Craps! You Lost!");
                roll_count = 0;
            }
            else{
                roll_count++;
                message_v.setText("Roll for " + Integer.toString(point) + "!");
            }
        }
    }
}
